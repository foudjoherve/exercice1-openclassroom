import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  @Input() public postTitle : string;
  @Input() public content : string;
  @Input() public loveIts : number;
  @Input() public created_at : string;

  onLoveIt() {
    this.loveIts++;
}

onDontLoveIt() {
    this.loveIts--;
}

}
