import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  pTitle = 'Mon exercice angular';

   constructor() {

   }

  posts = [
    {
    title: "Mon premier post",
  	content: "j'ai effectuer une randonner hier, elle étatit pas mal. mais je me suis un peux ennuyer par ce que j'étais seul",
  	loveIts: 3,
  	created_at: "09-11-2018"
    },
    {
     title: "Mon deuxième post",
  	content: "quand a moi j'ai fais les boutique, j'ai trouver plein d'articles original; il faut absolument que tu vois ca. je t'ai même prit quelques articles",
  	loveIts: 13,
  	created_at: "10-11-2018"
    },
    {
     title: "Mon troisième post",
  	content: "Apply display utilities to create a flexbox container and transform direct children elements into flex items. Flex containers and items are able to be modified",
  	loveIts: 0,
  	created_at: "08-11-2018"
    }
  ];
  
}
